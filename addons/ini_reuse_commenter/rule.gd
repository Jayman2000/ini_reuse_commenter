# SPDX-FileCopyrightText: 2021 Jason Yundt <swagfortress@gmail.com>
# SPDX-License-Identifier: CC0-1.0

extends Reference


const REUSE_TAG_TEMPLATE = "; SPDX-%s: %s\n"
var regex
var file_copyright_texts
var license_identifiers
var parse_as_config


func _init(pattern, file_copyright_texts, license_identifiers, parse_as_config):
	regex = RegEx.new()
	regex.compile(pattern)

	self.file_copyright_texts = file_copyright_texts
	self.license_identifiers = license_identifiers
	self.parse_as_config = parse_as_config


# Returns true if the file matches. A file matches if its path is a regex match
# or if its path contains a regex match.
func matches(path):
	return regex.search(path) != null


# Returns what should be added to the tops of matching files. Returns a string.
func header():
	var return_value = ""

	for value in file_copyright_texts:
		return_value += REUSE_TAG_TEMPLATE % ["FileCopyrightText", value]
	for value in license_identifiers:
		return_value += REUSE_TAG_TEMPLATE % ["License-Identifier", value]

	return return_value
