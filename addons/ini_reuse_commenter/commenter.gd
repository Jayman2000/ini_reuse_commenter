# SPDX-FileCopyrightText: 2021 Jason Yundt <swagfortress@gmail.com>
# SPDX-License-Identifier: CC0-1.0

tool
extends EditorPlugin
# Adds REUSE <https://reuse.software> comments to the top of specified INI
# files. Originally based on an answer by Wakatta:
# https://godotengine.org/qa/95921/can-i-include-reuse-comments-in-project-godot?show=96192#a96192


const CONFIG_PATH = "res://addons/ini_reuse_commenter/to_comment.cfg"
const EXIST_WARNING_TEMPLATE = 'WARNING: "%s" doesn\'t exist.'
const COULDNT_TEMPLATE = 'ERROR: Couldn\'t %s "%s". Error code %s.'
const OPEN_DIR_ERROR_TEMPLATE = 'ERROR: Couldn\'t open this directory: "%s". Error code %s.'
const OPEN_FILE_ERROR_TEMPLATE = 'ERROR: Failed to open "%s" for %s. Error code: %s.'
const Rule = preload("res://addons/ini_reuse_commenter/rule.gd")


func save_external_data():
	var rules = cfg_to_rules(load_cfg())

	var rules_map = generate_rules_map(rules)
	for path in rules_map:
		var header = ""
		var parse_as_config = false
		for rule in rules_map[path]:
			header += rule.header()
			parse_as_config = parse_as_config or rule.parse_as_config

		add_header(path, header, parse_as_config)


# Resets an INI file's comments, and adds header to the top of it.
#
# For most INI files, when comments are reset, all comments are removed. For
# project.godot, when comments are reset, all comments are removed, and a new
# comment is added at the top.
#
# path - A string that is the path to an INI file.
# header - A string that will be added to the top of the file.
# parse_as_config - If true, the file will be parsed using ConfigFile. This is
#                   required to strip comments out of .cfg files.
func add_header(path, header, parse_as_config):
	if parse_as_config:

		var config_file = ConfigFile.new()
		var error_code = config_file.load(path)
		if error_code == OK:

			error_code = config_file.save(path)
			if error_code != OK:
				printerr(COULDNT_TEMPLATE % ["save", path, error_code])

		else:
			printerr(COULDNT_TEMPLATE % ["load", path, error_code])

	var file = File.new()
	var error_code = file.open(path, File.READ)

	if error_code == OK:
		var text = header + file.get_as_text()
		file.close()

		error_code = file.open(path, File.WRITE)
		if error_code == OK:
			file.store_string(text)
		else:
			printerr(OPEN_FILE_ERROR_TEMPLATE % [path, "writing", error_code])
	else:
		printerr(OPEN_FILE_ERROR_TEMPLATE % [path, "reading",  error_code])

	file.close()


# Returns an array containing every single file in the given folder, including
# ones that are in one or more subfolders.
#
# path - Path to the given folder. Make sure it has a final slash.
# Return value: an array of strings. Each string is a path to a unique file.
func all_files_in(path):
	var return_value = []

	var dir = Directory.new()
	var error_code = dir.open(path)
	if error_code == OK:
		dir.list_dir_begin(true, true)  # TODO: What counts as a hidden file?

		var item_name = dir.get_next()
		while item_name != "":
			var absolute_path = path + item_name

			if dir.file_exists(item_name):
				return_value.append(absolute_path)
			elif dir.dir_exists(item_name):
				absolute_path += "/"
				return_value += all_files_in(absolute_path)
			else:
				printerr(EXIST_WARNING_TEMPLATE % [item_name])

			item_name = dir.get_next()
		dir.list_dir_end()
	else:
		printerr(OPEN_DIR_ERROR_TEMPLATE % [path, error_code])

	return return_value


# Turns a ConfigFile into an Array of Rules.
#
# cfg - An INI REUSE Commenter ConfigFile.
# Return value: The data in cfg as an Array of Rules.
func cfg_to_rules(cfg):
	var return_value = []

	for section in cfg.get_sections():
		var to_add = Rule.new(
				cfg.get_value(section, "pattern"),
				cfg.get_value(section, "file_copyright_texts"),
				cfg.get_value(section, "license_identifiers"),
				cfg.get_value(section, "parse_as_config", false)
		)

		return_value.append(to_add)

	return return_value


# Returns a dictionary. Each key is a string that is a path to a file. Each
# value is an Array containing the Rules that apply to that file.
#
# rules - An Array of Rules.
func generate_rules_map(rules):
	var return_value = {}

	for path in all_files_in("res://"):
		for rule in rules:
			if rule.matches(path):
				if return_value.has(path):
					return_value[path] += rule
				else:
					return_value[path] = [rule]

	return return_value


# Returns a ConfigFile containing the config that should be used. Deals with
# issues like missing files.
func load_cfg():
	var file = File.new()
	var return_value

	if file.file_exists(CONFIG_PATH):
		return_value = ConfigFile.new()
		var error_code = return_value.load(CONFIG_PATH)
		if error_code != OK:
			printerr(COULDNT_TEMPLATE % ["load", CONFIG_PATH, error_code])
	else:
		return_value = write_example_cfg()

	return return_value


# Create a file containing the default configuration.
#
# Return value: the ConfigFile that was written.
func write_example_cfg():
	var cfg = ConfigFile.new()

	cfg.set_value(
			"ini_reuse_commenter",
			"pattern",
			"res://addons/ini_reuse_commenter/.*\\.cfg"
	)
	cfg.set_value(
			"ini_reuse_commenter",
			"file_copyright_texts",
			["2021 Jason Yundt <swagfortress@gmail.com>"]
	)
	cfg.set_value(
			"ini_reuse_commenter",
			"license_identifiers",
			["CC0-1.0"]
	)
	cfg.set_value(
			"ini_reuse_commenter",
			"parse_as_config",
			true
	)

	cfg.save(CONFIG_PATH)
	return cfg
