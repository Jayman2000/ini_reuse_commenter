<!--SPDX-FileCopyrightText: 2021 Jason Yundt <swagfortress@gmail.com>
SPDX-License-Identifier: CC0-1.0
-->
# INI REUSE Commenter
Automatically add [REUSE](https://reuse.software/) comments to files like project.godot.

## Copying
This repo should be [REUSE](https://reuse.software/) compliant. For full copying information, install the [reuse tool](https://github.com/fsfe/reuse-tool), and run
`$ reuse spdx`

## Pre-commit
If you're going to contribute to this project, then you may want to have your contributions automatically checked. To do so,

1. Make sure that [pre-commit](https://pre-commit.com/) is installed.
1. `cd` to the root of the repo.
1. Run `$ pre-commit install`
